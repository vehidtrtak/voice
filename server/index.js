const express = require('express');
const app = express();
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

dotenv.config();
const port = process.env.PORT || 3200;
if (!process.env.JWT) {
    console.error('FATAL ERROR: jwtPrivateKey is not defined.');
    process.exit(1);
}

//connect to db
mongoose.connect(
    process.env.DB_CONNECT,{
        useUnifiedTopology: true,
        useNewUrlParser: true
    }).then(() => console.log('Successfully connected to the database...'))
    .catch(err => console.log('Could not connect to MongoDB...'));


//import Routes
const auth = require('./routes/auth');
const users = require('./routes/users');
const questions = require('./routes/questions');
const comments = require('./routes/comments');

//Route Middleware
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, x-auth-token");
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    next();
});
app.use(express.json());
app.use('/api/auth', auth);
app.use('/api/users', users);
app.use('/api/questions', questions)
app.use('/api/comments', comments)

//application port
app.listen(port, () => console.log(`The server is up and running on port ${port}`));

