import React from 'react';
import globalReducer from './reducers/globalReducer';

const initialState = {
  data: [
    // {
    //   id: 1,
    //   title: 'Programmatically navigate using react router',
    //   question: `With react-router I can use the Link element to create links which are natively handled by react router. I see internally it calls this.context.transitionTo(...). I want to do a navigation. Not …`,
    //   answers: [
    //     {
    //       id: 1,
    //       answerBody:
    //         'I have started to learn React out of curiosity and wanted to know the difference between React and React Native - though could not find a satisfactory answer using Google. React and React Native seems to have the same format. Do they have completely different syntax?',
    //       user: { name: 'trupet', id: 1 },
    //     },
    //     {
    //       id: 2,
    //       answerBody:
    //         'I have started to learn React out of curiosity and wanted to know the difference between React and React Native - though could not find a satisfactory answer using Google. React and React Native seems to have the same format. Do they have completely different syntax?',
    //       user: { name: 'song lee', id: 2 },
    //     },
    //     {
    //       id: 3,
    //       answerBody:
    //         'I have started to learn React out of curiosity and wanted to know the difference between React and React Native - though could not find a satisfactory answer using Google. React and React Native seems to have the same format. Do they have completely different syntax?',
    //       user: { name: 'kupret', id: 2 },
    //     },
    //   ],
    //   createdBy: 'maddox',
    //   dateCreated: '20.09.2020',
    //   user: {
    //     id: 100,
    //     isFavorited: true,
    //   },
    // },
    // {
    //   id: 2,
    //   title: 'Loop inside React JSX',
    //   question: `I'm trying to do something like the following in React JSX (where ObjectRow is a separate component): <tbody> for (var i=0; i < numrows; i++) { <ObjectRow/> } </tbody> I realize …`,
    //   answers: [],
    //   createdBy: 'kreon',
    //   dateCreated: '10.08.2020',
    //   user: {
    //     id: 101,
    //     isFavorited: true,
    //   },
    // },
    // {
    //   id: 3,
    //   title: 'What is the difference between React Native and React?',
    //   question: `I have started to learn React out of curiosity and wanted to know the difference between React and React Native - though could not find a satisfactory answer using Google. React and React Native seems to have the same format. Do they have completely different syntax? …`,
    //   answers: [],
    //   createdBy: 'maddox',
    //   dateCreated: '20.09.2020',
    //   user: {
    //     id: 100,
    //     isFavorited: false,
    //   },
    // },
  ],
};

const GlobalContext = React.createContext();

export const GlobalContextProvider = ({ children }) => {
  const [state, dispatch] = React.useReducer(globalReducer, initialState);

  return (
    <GlobalContext.Provider value={{ state, dispatch }}>
      {children}
    </GlobalContext.Provider>
  );
};

export default GlobalContext;
