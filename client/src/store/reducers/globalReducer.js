export const TOGGLE_FAVORITE = 'TOGGLE_FAVORITE';
export const ADD_ANSWER = 'ADD_ANSWER';
export const IS_AUTHENTICATED = 'IS_AUTHENTICATED';
export const QUESTION_LIST = 'QUESTION_LIST';
export const NEXT_PAGE = 'NEXT_PAGE';
export const ADD_QUESTION = 'ADD_QUESTION';

export default (state = [], action) => {
  switch (action.type) {
    case QUESTION_LIST:
      return { ...state, data: action.payload.data };
    case ADD_QUESTION:
      return { ...state, data: [...state.data, action.payload.question] };
    case TOGGLE_FAVORITE:
      return {
        ...state,
        data: state.data.map((item) => {
          if (item._id === action.payload.id) {
            return {
              ...item,
              isFavourite: !item.isFavourite,
            };
          }
          return item;
        }),
      };
    case ADD_ANSWER:
      return {
        ...state,
        data: state.data.map((item) => {
          if (item._id === action.payload.json.questionId) {
            return {
              ...item,
              commentsList: [...item.commentsList, action.payload.json],
              numberOfComments: item.commentsList.length + 1,
            };
          }
          return item;
        }),
      };
    case NEXT_PAGE:
      return {
        ...state,
        data: [...state.data.concat(action.payload.data)],
      };
    case IS_AUTHENTICATED:
      return { ...state, isAuthenticated: !state.isAuthenticated };
    default:
      return state;
  }
};
