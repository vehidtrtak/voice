import React from 'react';

const initialState = {
  isAuthenticated: localStorage.token ? true : false,
};

const AuthContext = React.createContext();

export const AuthContextProvider = ({ children }) => {
  const [state, setState] = React.useState(initialState);

  return (
    <AuthContext.Provider value={{ state, setState }}>
      {children}
    </AuthContext.Provider>
  );
};

export default AuthContext;
