import React from 'react';
import './QuestionList.css';
import { Question } from '../../components/Question';
import { fetchingGet } from '../../helpers/fetch';
import GlobalContext from '../../store/GlobalContext';

export const QuestionsList = () => {
  const { state, dispatch } = React.useContext(GlobalContext);
  const [showMore, setShowMore] = React.useState(true);
  const [handleError, setHandleError] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(true);
  const postsLimit = '5';
  const token = localStorage.getItem('token') || '';

  React.useEffect(() => {
    const fetching = async () => {
      let { res, json } = await fetchingGet(
        process.env.REACT_APP_NOT_SECRET_CODE + '/questions/0/' + postsLimit,
        token
      );

      if (res?.ok) dispatch({ type: 'QUESTION_LIST', payload: { data: json } });
      if (!res) setHandleError(!handleError);
      setIsLoading(false);
    };
    fetching();

    return () => {
      dispatch({ type: 'QUESTION_LIST', payload: { data: [] } });
    };
  }, []);

  const handleLoadMore = async (e) => {
    e.preventDefault();
    let { res, json } = await fetchingGet(
      process.env.REACT_APP_NOT_SECRET_CODE +
        '/questions/' +
        state.data.length +
        '/' +
        postsLimit,
      token
    );
    if (res?.ok) dispatch({ type: 'NEXT_PAGE', payload: { data: json } });
    if (json?.length < postsLimit) setShowMore(!showMore);
  };

  return (
    <>
      <h2 style={{ textAlign: 'center' }}>Home</h2>
      {!handleError ? (
        !isLoading ? (
          state.data.map((question) => {
            return (
              <Question
                key={question._id}
                question={question}
                dispatch={dispatch}
              />
            );
          })
        ) : (
          <img
            className='loading-placeholder-icon'
            src='/loading.svg'
            alt='loading'
          />
        )
      ) : (
        <p className='loading-placeholder-text'>Issue with server</p>
      )}
      {showMore && (
        <button className='load-more' onClick={handleLoadMore}>
          Load more...
        </button>
      )}
    </>
  );
};
