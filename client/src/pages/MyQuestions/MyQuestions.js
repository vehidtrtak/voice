import React from 'react';
import GlobalContext from '../../store/GlobalContext';
import { fetchingPost, fetchingGet } from '../../helpers/fetch';
import { Question } from '../../components/Question';
import { CommentBox } from '../../components/CommentBox';
export const MyQuestions = () => {
  const { state, dispatch } = React.useContext(GlobalContext);
  const questionRef = React.useRef(null);
  const [handleError, setHandleError] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(true);
  const postsLimit = '5';
  const questionTitleRef = React.useRef(null);

  const token = localStorage.getItem('token') || '';

  React.useEffect(() => {
    const fetching = async () => {
      let { res, json } = await fetchingGet(
        process.env.REACT_APP_NOT_SECRET_CODE +
          '/questions/myquestions/0/' +
          postsLimit,
        token
      );
      if (res?.ok) dispatch({ type: 'QUESTION_LIST', payload: { data: json } });

      if (!res) setHandleError(!handleError);

      setIsLoading(false);
    };
    fetching();

    return () => {
      dispatch({ type: 'QUESTION_LIST', payload: { data: [] } });
    };
  }, []);

  const handleClick = async () => {
    const body = {
      questionTitle: questionTitleRef.current.value,
      questionBody: questionRef.current.value,
    };
    let { res, json } = await fetchingPost(
      process.env.REACT_APP_NOT_SECRET_CODE + '/questions',
      token,
      body
    );
    if (res?.ok) {
      dispatch({
        type: 'ADD_QUESTION',
        payload: {
          question: json,
        },
      });
    }

    questionRef.current.value = '';
    questionTitleRef.current.value = '';
  };

  return (
    <>
      <h2 style={{ textAlign: 'center' }}>My Questions</h2>
      {!isLoading ? (
        state.data.map((question) => (
          <Question
            key={question._id}
            question={question}
            dispatch={dispatch}
          />
        ))
      ) : (
        <img
          className='loading-placeholder-icon'
          src='/loading.svg'
          alt='loading'
        />
      )}
      <CommentBox
        question={questionRef}
        questionTitle={questionTitleRef}
        handleClick={handleClick}
        boxTitle='Add a question'
      />
    </>
  );
};
