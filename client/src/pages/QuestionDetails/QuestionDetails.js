import React from 'react';
import './QuestionDetails.css';
import GlobalContext from '../../store/GlobalContext';
import { fetchingPost, fetchingGet } from '../../helpers/fetch';
import { Question } from '../../components/Question';
import { CommentBox } from '../../components/CommentBox';
import { useParams } from 'react-router-dom';

export const QuestionDetails = () => {
  const { state, dispatch } = React.useContext(GlobalContext);
  const questionRef = React.useRef(null);
  const [handleError, setHandleError] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(true);
  const { id } = useParams();
  const token = localStorage.getItem('token');

  React.useEffect(() => {
    const fetching = async () => {
      let { res, json } = await fetchingGet(
        process.env.REACT_APP_NOT_SECRET_CODE + '/questions/' + id,
        token
      );

      if (res?.ok)
        dispatch({ type: 'QUESTION_LIST', payload: { data: [json] } });

      if (!res) setHandleError(!handleError);
      setIsLoading(false);
    };
    fetching();

    return () => {
      dispatch({ type: 'QUESTION_LIST', payload: { data: [] } });
    };
  }, []);

  const handleClick = async () => {
    const body = {
      questionId: id,
      commentBody: questionRef.current.value,
    };

    if (questionRef.current.value === '') {
      setHandleError("Can't sent empty answer");
      return;
    } else setHandleError('');

    let { res, json } = await fetchingPost(
      process.env.REACT_APP_NOT_SECRET_CODE + '/comments',
      token,
      body
    );
    if (res?.ok)
      dispatch({
        type: 'ADD_ANSWER',
        payload: {
          json,
        },
      });

    questionRef.current.value = '';
  };

  return (
    <>
      {!isLoading ? (
        <>
          {state.data.map((question) => {
            return (
              <Question
                key={question._id}
                question={question}
                dispatch={dispatch}
              />
            );
          })}
          <div className='QuestionDetails-answers'>
            <ul>
              {state.data[0].commentsList.map((answer) => {
                return (
                  <div key={answer._id}>
                    <li>{answer.commentBody}</li>
                    <p className='QuestionDetails-footer'>~{answer.userName}</p>
                  </div>
                );
              })}
            </ul>
          </div>
        </>
      ) : (
        <img
          className='loading-placeholder-icon'
          src='/loading.svg'
          alt='loading'
        />
      )}
      <CommentBox
        question={questionRef}
        handleClick={handleClick}
        boxTitle='Add an answer'
        handleError={handleError}
      />
    </>
  );
};
