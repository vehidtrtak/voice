import React from 'react';
import { fetchingPost } from '../../helpers/fetch';
import { Link, useHistory } from 'react-router-dom';
import '../Login/Login.css';

export const Register = () => {
  const history = useHistory();
  const [name, setName] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [inputError, setInputError] = React.useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();
    const token = '';
    const body = {
      email: email,
      password: password,
      name: name,
    };
    const { res, json } = await fetchingPost(
      process.env.REACT_APP_NOT_SECRET_CODE + '/users/register',
      token,
      body
    );

    if (res.ok) history.push('/login');
    if (json.message) setInputError(json.message);
  };

  return (
    <div className='Login-container'>
      <div className='Login-form'>
        <div className='Login-logo'>#</div>
        <form onSubmit={handleSubmit}>
          <h3 className='Login-title'>Register</h3>
          <i className='is-error'>{inputError}</i>
          <label htmlFor='name'>Name:</label>
          <input
            type='text'
            id='name'
            onChange={(e) => setName(e.target.value)}
          />
          <label htmlFor='email'>Email:</label>
          <input
            type='text'
            id='email'
            onChange={(e) => setEmail(e.target.value)}
          />
          <label htmlFor='password'>Password:</label>
          <input
            type='password'
            id='password'
            onChange={(e) => setPassword(e.target.value)}
          />
          <Link to='/login'>Go to login</Link>
          <input type='submit' value='Register' />
        </form>
      </div>
    </div>
  );
};
