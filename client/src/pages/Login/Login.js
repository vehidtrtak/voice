import React from 'react';
import './Login.css';
import { fetchingPost } from '../../helpers/fetch';
import AuthContext from '../../store/AuthContext';
import { Link } from 'react-router-dom';

export const Login = () => {
  const { setState } = React.useContext(AuthContext);
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [inputError, setInputError] = React.useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();
    const token = '';
    const body = {
      email: email,
      password: password,
    };
    let { res, json } = await fetchingPost(
      process.env.REACT_APP_NOT_SECRET_CODE + '/auth',
      token,
      body
    );
    if (res?.ok) {
      localStorage.setItem('token', JSON.stringify(json.token));
      setState((prevState) => {
        return {
          ...prevState,
          isAuthenticated: !prevState.isAuthenticated,
        };
      });
    }
    if (json?.message) setInputError(json.message);
  };

  return (
    <div className='Login-container'>
      <div className='Login-form'>
        <div className='Login-logo'>#</div>
        <form onSubmit={handleSubmit}>
          <h3 className='Login-title'>Login</h3>
          <i className='is-error'>{inputError}</i>
          <label htmlFor='email'>Email:</label>
          <input
            type='text'
            id='email'
            onChange={(e) => setEmail(e.target.value)}
          />
          <label htmlFor='password'>Password:</label>
          <input
            type='password'
            onChange={(e) => setPassword(e.target.value)}
            id='password'
          />
          <Link to='/register'>Register account</Link>
          <input type='submit' value='Login' />
        </form>
      </div>
    </div>
  );
};
