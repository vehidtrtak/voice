import React from 'react';
import './App.css';
import { GlobalContextProvider } from './store/GlobalContext';
import { AuthContextProvider } from './store/AuthContext';
import { RouteAuth } from './components/RouteAuth';

function App() {
  return (
    <div className='App'>
      <AuthContextProvider>
        <GlobalContextProvider>
          <RouteAuth />
        </GlobalContextProvider>
      </AuthContextProvider>
    </div>
  );
}

export default App;
