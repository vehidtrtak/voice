export const fetchingPost = async (url, token = '', body = '') => {
  const options = {
    method: 'POST',
    withCredentials: true,
    mode: 'cors',
    headers: {
      'X-Auth-Token': token ? token?.replace(/"/g, '') : '',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body),
  };
  try {
    const res = await fetch(url, options);
    const json = await res.json();
    return { res, json };
  } catch (error) {
    console.log(error);
    return 'Server error';
  }
};

export const fetchingGet = async (url, token) => {
  const options = {
    method: 'GET',
    mode: 'cors',
    withCredentials: true,
    headers: {
      'X-Auth-Token': token?.replace(/"/g, ''),
      'Content-Type': 'application/json',
    },
  };
  try {
    const res = await fetch(url, options);
    const json = await res.json();
    return { res, json };
  } catch (error) {
    console.log(error);
    return 'Server error';
  }
};
