import React from 'react';
import { NavLink, useHistory, useLocation } from 'react-router-dom';
import './Navbar.css';
import AuthContext from '../../store/AuthContext';
import GlobalContext from '../../store/GlobalContext';
import { fetchingPost } from '../../helpers/fetch';

export const Navbar = () => {
  const { setState } = React.useContext(AuthContext);
  const { dispatch } = React.useContext(GlobalContext);
  const [showDropdown, setShowDropdown] = React.useState(false);
  const [handleError, setHandleError] = React.useState(false);
  const location = useLocation();
  const searchRef = React.useRef('');
  const token = localStorage.getItem('token');

  const handleClick = (e) => {
    setShowDropdown(!showDropdown);
  };

  const handleLogout = () => {
    setState((prevState) => {
      return {
        ...prevState,
        isAuthenticated: !prevState.isAuthenticated,
      };
    });
    localStorage.removeItem('token');
  };

  const handleKeyPress = async (e) => {
    const body = {
      query: searchRef.current.value,
    };
    if (e.key === 'Enter') {
      let { res, json } = await fetchingPost(
        process.env.REACT_APP_NOT_SECRET_CODE + '/questions/search',
        token,
        body
      );
      if (res?.ok) dispatch({ type: 'QUESTION_LIST', payload: { data: json } });
      if (!res) setHandleError(!handleError);
    }
  };

  return (
    <header className='Navbar-container'>
      <div className='Navbar-content'>
        <div className='Navbar-logo'>
          <img src='/voice-logo-light.png' alt='voice' />
        </div>
        {location.pathname === '/' && (
          <input
            className='Navbar-search'
            type='text'
            ref={searchRef}
            onKeyPress={handleKeyPress}
          />
        )}
        <div className='mobile-menu'>
          <img src='/menu.png' onClick={handleClick} alt='menu-icon' />
          <ul style={{ display: showDropdown ? 'flex' : 'none' }}>
            <li className='menu-trianlge'></li>
            <li>
              <NavLink exact to='/'>
                Home
              </NavLink>
            </li>
            <li>
              <NavLink to='/myquestions'>My Questions</NavLink>
            </li>
            <li onClick={handleLogout}>Logout</li>
          </ul>
        </div>
        <ul>
          <li>
            <NavLink exact to='/'>
              Home
            </NavLink>
          </li>
          <li>
            <NavLink to='/myquestions'>My Questions</NavLink>
          </li>
          <li onClick={handleLogout}>
            <img src='/logout.svg' alt='logout' />
          </li>
        </ul>
      </div>
    </header>
  );
};
