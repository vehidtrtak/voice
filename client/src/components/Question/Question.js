import React from 'react';
import './Question.css';
import { useHistory } from 'react-router-dom';

export const Question = ({ question, dispatch }) => {
  let history = useHistory();

  const handleClick = (id) => {
    history.push('/question/' + id);
  };

  const formatDate = (date) => {
    const d = new Date(date);
    const ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d);
    const mo = new Intl.DateTimeFormat('en', { month: '2-digit' }).format(d);
    const da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d);
    return `${da}.${mo}.${ye}`;
  };

  return (
    <div className='Question-container'>
      <div className='Question-header'>
        <h4 onClick={() => handleClick(question._id)}>
          {question.questionTitle}
        </h4>
      </div>
      <div className='Question-text'>{question.questionBody}</div>
      <div className='Question-footer'>
        {question.numberOfComments || 0} answers
        <div className='Question-details'>
          <img src='/user-icon.svg' className='user-image' alt='user' />
          <div className='user-details'>
            <p>{question.userName}</p>
            <i>{formatDate(question.dateCreated)}</i>
          </div>
        </div>
      </div>
    </div>
  );
};
