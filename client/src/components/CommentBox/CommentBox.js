import React from 'react';
import './CommentBox.css';

export const CommentBox = ({
  question,
  handleClick,
  questionTitle,
  boxTitle,
  handleError,
}) => {
  return (
    <div className='QuestionDetails-add-answer'>
      <h4>{boxTitle}</h4>
      <i className='is-error'>{handleError}</i>
      {questionTitle && (
        <>
          <input
            type='text'
            className='QuestionDetails-answers-title'
            placeholder='Title'
            ref={questionTitle}
          />
        </>
      )}
      <textarea
        id='QuestionDetails-textarea'
        name='question'
        ref={question}
      ></textarea>
      <input type='submit' value='Add' onClick={handleClick} />
    </div>
  );
};
