import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import { Navbar } from '../Navbar';
import { QuestionsList } from '../../pages/QuestionsList';
import { Login } from '../../pages/Login';
import { Register } from '../../pages/Register';
import { MyQuestions } from '../../pages/MyQuestions';
import { QuestionDetails } from '../../pages/QuestionDetails';
import AuthContext from '../../store/AuthContext';

export const RouteAuth = () => {
  const { state } = React.useContext(AuthContext);

  const LoginContainer = ({ ...rest }) => (
    <Route
      {...rest}
      render={({ location }) =>
        !state.isAuthenticated ? (
          <Login />
        ) : (
          <Redirect
            to={{
              pathname: '/',
              state: { from: location },
            }}
          />
        )
      }
    />
  );

  const RegisterContainer = ({ ...rest }) => (
    <Route
      {...rest}
      render={({ location }) =>
        !state.isAuthenticated ? (
          <Register />
        ) : (
          <Redirect
            to={{
              pathname: '/',
              state: { from: location },
            }}
          />
        )
      }
    />
  );

  const DefaultContainer = () => (
    <>
      <Navbar />
      <div className='App-container'>
        <AuthRoute path='/' exact>
          <QuestionsList />
        </AuthRoute>
        <AuthRoute path='/myquestions'>
          <MyQuestions />
        </AuthRoute>
        <AuthRoute path='/question/:id'>
          <QuestionDetails />
        </AuthRoute>
      </div>
    </>
  );

  const AuthRoute = ({ children, ...rest }) => {
    return (
      <Route
        {...rest}
        render={({ location }) =>
          state.isAuthenticated ? (
            children
          ) : (
            <Redirect
              to={{
                pathname: '/login',
                state: { from: location },
              }}
            />
          )
        }
      />
    );
  };

  return (
    <Router>
      <Switch>
        <Route exact path='/login' component={LoginContainer} />
        <Route exact path='/register' component={RegisterContainer} />
        <Route component={DefaultContainer} />
      </Switch>
    </Router>
  );
};
