# Voice

Voice is an application made with React, NodeJS and MongoDB for database.

The application has a registration page, where users can register by providing name, email and password.
After successful registration, new users is redirected to the login page, where using their registered credentials can login to the application.

On the **"Home"** page user can see limited number of questions, and when they reach the end of the page, a **"Load more"** button can be used to load more questions. When clicked on the question title, new page appears with the question we clicked on and all of it's answers. Also there is an option to add a new answer to that question on the bottom of the page.

Next we have a **"My Questions"** page where user can see the questions they created. At the bottom of the page user can add a new question.

On the menu bar there is a **search bar** where user can search for questions by question title.

## Table of contents

- [Technologies](#technologies)
- [Dependencies](#dependencies)
- [Installation](#installation)
- [Screenshots](#screenshots)

## Technologies

- ReactJS v16.13.1
- NodeJS v10.16.3
- MongoDB v4.2
- Express v4.17.1

## Dependencies

- NodeJS >= 10.16.3
- npm >= 6.9.0
- MongoDB >= 4.2

## Installation

**Backend**

In the server folder first install all the dependencies by running:

```bash
npm install
```

To start the server application on the localhost:3200 run:

```bash
npm start
```

If that port is taken another port will be chosen.

**Frontend**

In the client folder first install all the dependencies by running:

```bash
npm install
```

To start the client application on the localhost:3000 run:

```bash
npm start
```

If that port is taken another port will be chosen.

After the application is started, new window on your default browser will open with that application.If it doesn't open you can go manually enter in your browser the URL:

```bash
http://192.168.0.16:3000/
```

## Screenshots

![Login page](./client/public/readme-img/login.png)

**Login page**

![Register page](./client/public/readme-img/register.png)

**Register page**

![Home page](./client/public/readme-img/homepage.png)

**Home page**

![My Questions page](./client/public/readme-img/myquestions.png)

**My Questions page**

![Question (Details) page](./client/public/readme-img/question.png)

**Question page**
